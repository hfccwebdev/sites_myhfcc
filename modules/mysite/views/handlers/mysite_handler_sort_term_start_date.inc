<?php

/**
 * Return term start date sorting info.
 *
 * @ingroup views_sort_handlers
 */
class mysite_handler_sort_term_start_date extends views_handler_sort {

  /**
   * Add ability to sort view by term start dates.
   *
   * @see http://dev.mysql.com/doc/refman/5.5/en/string-functions.html#function_field
   * @see views_handler_sort_date::query()
   * @see views_plugin_query_default::add_orderby()
   */
  function query() {
    $this->ensure_my_table();
    // Results of WebServicesClient::getTerms() are sorted by term_start_date.
    $terms = WebServicesClient::getTerms();
    $field = $this->table_alias . "." . $this->real_field;
    foreach (array_keys($terms) as $key) {
      $field .= ", '" . $key . "'";
    }
    $fakefield = "FIELD(" . $field . ")";
    $this->query->add_orderby(NULL, $fakefield, $this->options['order'], $this->table_alias . '_term_start_date');
  }
}
