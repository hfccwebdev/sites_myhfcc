<?php

/**
 * Return the term start date.
 *
 * @ingroup views_field_handlers
 */
class mysite_handler_field_term_start_date extends views_handler_field {

  /**
  * Render the field.
  *
  * @todo Add date formatters. This just shows raw serial date.
  */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      $terms = WebServicesClient::getTerms();
      return !empty($terms[$value]) ? $terms[$value]['term_start_date'] : NULL;
    }
  }
}
