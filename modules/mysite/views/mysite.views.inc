<?php

/**
 * @file
 * Views handlers for mysite.module
 */

/**
 * Implements hook_views_data_alter().
 */
function mysite_views_data_alter(&$data) {

  // Add a dummy field on field_data_field_term that will
  // allow use of the term_start_date via hfccwsclient.
  $data['field_data_field_term']['field_term_start_date'] = [
    'group' => t('Content'),
    'title' => t('Term Start Date'),
    'help' => t('Return the term start date.'),
    'real field' => 'field_term_value',
    'field' => ['handler' => 'mysite_handler_field_term_start_date', 'click sortable' => FALSE],
    'sort' => ['handler' => 'mysite_handler_sort_term_start_date'],
  ];
}
