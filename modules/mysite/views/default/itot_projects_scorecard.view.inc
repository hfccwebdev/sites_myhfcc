<?php

$view = new view();
$view->name = 'itot_projects_scorecard';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'ITOT Projects Scorecard';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'ITOT Committee Projects Scorecard';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  4 => '4',
  31 => '31',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'field_project_health' => 'field_project_health',
  'field_weight' => 'field_weight',
  'title' => 'title',
  'field_project_charter' => 'field_project_charter',
  'field_project_owner' => 'field_project_owner',
  'field_start_date' => 'field_start_date',
  'field_expected_end_date' => 'field_expected_end_date',
  'field_project_progress' => 'field_project_progress',
  'field_project_lead' => 'field_project_lead',
  'field_project_its_contact' => 'field_project_its_contact',
  'body' => 'body',
  'field_project_tracking' => 'body',
  'changed' => 'body',
  'edit_node' => 'edit_node',
);
$handler->display->display_options['style_options']['class'] = '';
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_project_health' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_weight' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_project_charter' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => 'views-align-center',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_project_owner' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_start_date' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_expected_end_date' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_project_progress' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_project_lead' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_project_its_contact' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'body' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => ' | ',
    'empty_column' => 0,
  ),
  'field_project_tracking' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'changed' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['sticky'] = TRUE;
/* Field: Content: Project Health */
$handler->display->display_options['fields']['field_project_health']['id'] = 'field_project_health';
$handler->display->display_options['fields']['field_project_health']['table'] = 'field_data_field_project_health';
$handler->display->display_options['fields']['field_project_health']['field'] = 'field_project_health';
$handler->display->display_options['fields']['field_project_health']['label'] = 'Project Status';
$handler->display->display_options['fields']['field_project_health']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_project_health']['alter']['text'] = '<img src="/sites/hfcportal/themes/portal/images/project-status-[field_project_health-value].png" alt="[field_project_health]" title="[field_project_health]">';
/* Field: Content: Order */
$handler->display->display_options['fields']['field_weight']['id'] = 'field_weight';
$handler->display->display_options['fields']['field_weight']['table'] = 'field_data_field_weight';
$handler->display->display_options['fields']['field_weight']['field'] = 'field_weight';
$handler->display->display_options['fields']['field_weight']['label'] = '';
$handler->display->display_options['fields']['field_weight']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_weight']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_weight']['type'] = 'number_unformatted';
$handler->display->display_options['fields']['field_weight']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Field: Rank */
$handler->display->display_options['fields']['expression']['id'] = 'expression';
$handler->display->display_options['fields']['expression']['table'] = 'views';
$handler->display->display_options['fields']['expression']['field'] = 'expression';
$handler->display->display_options['fields']['expression']['ui_name'] = 'Rank';
$handler->display->display_options['fields']['expression']['label'] = 'Rank';
$handler->display->display_options['fields']['expression']['precision'] = '0';
$handler->display->display_options['fields']['expression']['expression'] = '[field_weight] + 1';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Project Charter */
$handler->display->display_options['fields']['field_project_charter']['id'] = 'field_project_charter';
$handler->display->display_options['fields']['field_project_charter']['table'] = 'field_data_field_project_charter';
$handler->display->display_options['fields']['field_project_charter']['field'] = 'field_project_charter';
$handler->display->display_options['fields']['field_project_charter']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_project_charter']['alter']['text'] = 'Y';
$handler->display->display_options['fields']['field_project_charter']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_project_charter']['alter']['path'] = '[field_project_charter-url]';
$handler->display->display_options['fields']['field_project_charter']['alter']['target'] = '_blank';
$handler->display->display_options['fields']['field_project_charter']['empty'] = 'N';
$handler->display->display_options['fields']['field_project_charter']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_project_charter']['type'] = 'link_absolute';
/* Field: Content: Project Owner */
$handler->display->display_options['fields']['field_project_owner']['id'] = 'field_project_owner';
$handler->display->display_options['fields']['field_project_owner']['table'] = 'field_data_field_project_owner';
$handler->display->display_options['fields']['field_project_owner']['field'] = 'field_project_owner';
/* Field: Content: Start Date */
$handler->display->display_options['fields']['field_start_date']['id'] = 'field_start_date';
$handler->display->display_options['fields']['field_start_date']['table'] = 'field_data_field_start_date';
$handler->display->display_options['fields']['field_start_date']['field'] = 'field_start_date';
$handler->display->display_options['fields']['field_start_date']['settings'] = array(
  'format_type' => 'calendar_short',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Deadline */
$handler->display->display_options['fields']['field_expected_end_date']['id'] = 'field_expected_end_date';
$handler->display->display_options['fields']['field_expected_end_date']['table'] = 'field_data_field_expected_end_date';
$handler->display->display_options['fields']['field_expected_end_date']['field'] = 'field_expected_end_date';
$handler->display->display_options['fields']['field_expected_end_date']['settings'] = array(
  'format_type' => 'calendar_short',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Progress Indicator */
$handler->display->display_options['fields']['field_project_progress']['id'] = 'field_project_progress';
$handler->display->display_options['fields']['field_project_progress']['table'] = 'field_data_field_project_progress';
$handler->display->display_options['fields']['field_project_progress']['field'] = 'field_project_progress';
$handler->display->display_options['fields']['field_project_progress']['label'] = 'Current Status';
/* Field: Content: Project Lead */
$handler->display->display_options['fields']['field_project_lead']['id'] = 'field_project_lead';
$handler->display->display_options['fields']['field_project_lead']['table'] = 'field_data_field_project_lead';
$handler->display->display_options['fields']['field_project_lead']['field'] = 'field_project_lead';
$handler->display->display_options['fields']['field_project_lead']['label'] = 'Project Leader';
/* Field: Content: ITS Contact */
$handler->display->display_options['fields']['field_project_its_contact']['id'] = 'field_project_its_contact';
$handler->display->display_options['fields']['field_project_its_contact']['table'] = 'field_data_field_project_its_contact';
$handler->display->display_options['fields']['field_project_its_contact']['field'] = 'field_project_its_contact';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = 'Summary';
$handler->display->display_options['fields']['body']['alter']['max_length'] = '500';
$handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
$handler->display->display_options['fields']['body']['alter']['preserve_tags'] = '<a> <strong> <em>';
/* Field: Content: Issue Tracker */
$handler->display->display_options['fields']['field_project_tracking']['id'] = 'field_project_tracking';
$handler->display->display_options['fields']['field_project_tracking']['table'] = 'field_data_field_project_tracking';
$handler->display->display_options['fields']['field_project_tracking']['field'] = 'field_project_tracking';
$handler->display->display_options['fields']['field_project_tracking']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_project_tracking']['settings'] = array(
  'custom_title' => 'Details',
);
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['changed']['alter']['text'] = 'Updated: [changed]';
$handler->display->display_options['fields']['changed']['date_format'] = 'calendar_short';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
/* Sort criterion: Content: Order (field_weight) */
$handler->display->display_options['sorts']['field_weight_value']['id'] = 'field_weight_value';
$handler->display->display_options['sorts']['field_weight_value']['table'] = 'field_data_field_weight';
$handler->display->display_options['sorts']['field_weight_value']['field'] = 'field_weight_value';
/* Sort criterion: Content: Start Date (field_start_date) */
$handler->display->display_options['sorts']['field_start_date_value']['id'] = 'field_start_date_value';
$handler->display->display_options['sorts']['field_start_date_value']['table'] = 'field_data_field_start_date';
$handler->display->display_options['sorts']['field_start_date_value']['field'] = 'field_start_date_value';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'itot_projects_scorecard:page_1';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
/* Contextual filter: Content: Project Type (field_project_type) */
$handler->display->display_options['arguments']['field_project_type_value']['id'] = 'field_project_type_value';
$handler->display->display_options['arguments']['field_project_type_value']['table'] = 'field_data_field_project_type';
$handler->display->display_options['arguments']['field_project_type_value']['field'] = 'field_project_type_value';
$handler->display->display_options['arguments']['field_project_type_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_project_type_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_project_type_value']['default_argument_options']['argument'] = 'itot';
$handler->display->display_options['arguments']['field_project_type_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_project_type_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_project_type_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_project_type_value']['limit'] = '0';
$handler->display->display_options['filter_groups']['operator'] = 'OR';
$handler->display->display_options['filter_groups']['groups'] = array(
  1 => 'AND',
  2 => 'AND',
);
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'it_project' => 'it_project',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Progress Indicator (field_project_progress) */
$handler->display->display_options['filters']['field_project_progress_value']['id'] = 'field_project_progress_value';
$handler->display->display_options['filters']['field_project_progress_value']['table'] = 'field_data_field_project_progress';
$handler->display->display_options['filters']['field_project_progress_value']['field'] = 'field_project_progress_value';
$handler->display->display_options['filters']['field_project_progress_value']['operator'] = 'not';
$handler->display->display_options['filters']['field_project_progress_value']['value'] = array(
  'G' => 'G',
  'H' => 'H',
);
$handler->display->display_options['filters']['field_project_progress_value']['group'] = 1;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status_1']['id'] = 'status_1';
$handler->display->display_options['filters']['status_1']['table'] = 'node';
$handler->display->display_options['filters']['status_1']['field'] = 'status';
$handler->display->display_options['filters']['status_1']['value'] = '1';
$handler->display->display_options['filters']['status_1']['group'] = 2;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type_1']['id'] = 'type_1';
$handler->display->display_options['filters']['type_1']['table'] = 'node';
$handler->display->display_options['filters']['type_1']['field'] = 'type';
$handler->display->display_options['filters']['type_1']['value'] = array(
  'it_project' => 'it_project',
);
$handler->display->display_options['filters']['type_1']['group'] = 2;
/* Filter criterion: Content: Progress Indicator (field_project_progress) */
$handler->display->display_options['filters']['field_project_progress_value_1']['id'] = 'field_project_progress_value_1';
$handler->display->display_options['filters']['field_project_progress_value_1']['table'] = 'field_data_field_project_progress';
$handler->display->display_options['filters']['field_project_progress_value_1']['field'] = 'field_project_progress_value';
$handler->display->display_options['filters']['field_project_progress_value_1']['value'] = array(
  'G' => 'G',
  'H' => 'H',
);
$handler->display->display_options['filters']['field_project_progress_value_1']['group'] = 2;
/* Filter criterion: Content: Completion Date (field_end_date) */
$handler->display->display_options['filters']['field_end_date_value']['id'] = 'field_end_date_value';
$handler->display->display_options['filters']['field_end_date_value']['table'] = 'field_data_field_end_date';
$handler->display->display_options['filters']['field_end_date_value']['field'] = 'field_end_date_value';
$handler->display->display_options['filters']['field_end_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_end_date_value']['group'] = 2;
$handler->display->display_options['filters']['field_end_date_value']['default_date'] = '-4 weeks';

/* Display: ITOT Page */
$handler = $view->new_display('page', 'ITOT Page', 'page');
$handler->display->display_options['path'] = 'faculty-and-staff/its/projects/scorecard';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'ITOT Projects';
$handler->display->display_options['menu']['weight'] = '-10';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'IT Projects';
$handler->display->display_options['tab_options']['weight'] = '0';

/* Display: ITOT Sort */
$handler = $view->new_display('page', 'ITOT Sort', 'page_itot_sort');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'draggableviews' => 'draggableviews',
  'field_project_health' => 'field_project_health',
  'field_weight' => 'field_weight',
  'title' => 'title',
  'field_project_owner' => 'field_project_owner',
  'field_project_lead' => 'field_project_owner',
  'field_start_date' => 'field_start_date',
  'field_end_date' => 'field_end_date',
  'field_project_progress' => 'field_project_progress',
  'field_project_its_contact' => 'field_project_its_contact',
);
$handler->display->display_options['style_options']['class'] = '';
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'draggableviews' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_project_health' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_weight' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_project_owner' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '<br>',
    'empty_column' => 0,
  ),
  'field_project_lead' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_start_date' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_end_date' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_project_progress' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_project_its_contact' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['sticky'] = TRUE;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Draggableviews: Content */
$handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
$handler->display->display_options['fields']['draggableviews']['table'] = 'node';
$handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
$handler->display->display_options['fields']['draggableviews']['label'] = 'Sort';
$handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
$handler->display->display_options['fields']['draggableviews']['draggableviews']['handler'] = 'draggableviews_handler_fieldapi';
$handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
$handler->display->display_options['fields']['draggableviews']['draggableviews']['draggableviews_handler_fieldapi'] = array(
  'field' => 'field_data_field_weight:field_weight_value',
);
/* Field: Content: Project Health */
$handler->display->display_options['fields']['field_project_health']['id'] = 'field_project_health';
$handler->display->display_options['fields']['field_project_health']['table'] = 'field_data_field_project_health';
$handler->display->display_options['fields']['field_project_health']['field'] = 'field_project_health';
$handler->display->display_options['fields']['field_project_health']['label'] = 'Project Status';
$handler->display->display_options['fields']['field_project_health']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_project_health']['alter']['text'] = '<img src="/sites/hfcportal/themes/portal/images/project-status-[field_project_health-value].png" alt="[field_project_health]" title="[field_project_health]">';
/* Field: Content: Order */
$handler->display->display_options['fields']['field_weight']['id'] = 'field_weight';
$handler->display->display_options['fields']['field_weight']['table'] = 'field_data_field_weight';
$handler->display->display_options['fields']['field_weight']['field'] = 'field_weight';
$handler->display->display_options['fields']['field_weight']['label'] = 'Weight';
$handler->display->display_options['fields']['field_weight']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Project Owner */
$handler->display->display_options['fields']['field_project_owner']['id'] = 'field_project_owner';
$handler->display->display_options['fields']['field_project_owner']['table'] = 'field_data_field_project_owner';
$handler->display->display_options['fields']['field_project_owner']['field'] = 'field_project_owner';
/* Field: Content: Start Date */
$handler->display->display_options['fields']['field_start_date']['id'] = 'field_start_date';
$handler->display->display_options['fields']['field_start_date']['table'] = 'field_data_field_start_date';
$handler->display->display_options['fields']['field_start_date']['field'] = 'field_start_date';
$handler->display->display_options['fields']['field_start_date']['settings'] = array(
  'format_type' => 'calendar_short',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Completion Date */
$handler->display->display_options['fields']['field_end_date']['id'] = 'field_end_date';
$handler->display->display_options['fields']['field_end_date']['table'] = 'field_data_field_end_date';
$handler->display->display_options['fields']['field_end_date']['field'] = 'field_end_date';
$handler->display->display_options['fields']['field_end_date']['label'] = 'Deadline';
$handler->display->display_options['fields']['field_end_date']['settings'] = array(
  'format_type' => 'short',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Progress Indicator */
$handler->display->display_options['fields']['field_project_progress']['id'] = 'field_project_progress';
$handler->display->display_options['fields']['field_project_progress']['table'] = 'field_data_field_project_progress';
$handler->display->display_options['fields']['field_project_progress']['field'] = 'field_project_progress';
$handler->display->display_options['fields']['field_project_progress']['label'] = 'Current Status';
/* Field: Content: ITS Contact */
$handler->display->display_options['fields']['field_project_its_contact']['id'] = 'field_project_its_contact';
$handler->display->display_options['fields']['field_project_its_contact']['table'] = 'field_data_field_project_its_contact';
$handler->display->display_options['fields']['field_project_its_contact']['field'] = 'field_project_its_contact';
$handler->display->display_options['path'] = 'faculty-and-staff/its/projects/scorecard/sort';
$handler->display->display_options['menu']['title'] = 'ITOT Projects';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'IT Projects';
$handler->display->display_options['tab_options']['weight'] = '0';

/* Display: Featured Page */
$handler = $view->new_display('page', 'Featured Page', 'page_featured');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Non-ITOT Featured Projects';
$handler->display->display_options['enabled'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Project Type (field_project_type) */
$handler->display->display_options['arguments']['field_project_type_value']['id'] = 'field_project_type_value';
$handler->display->display_options['arguments']['field_project_type_value']['table'] = 'field_data_field_project_type';
$handler->display->display_options['arguments']['field_project_type_value']['field'] = 'field_project_type_value';
$handler->display->display_options['arguments']['field_project_type_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_project_type_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_project_type_value']['default_argument_options']['argument'] = 'featured';
$handler->display->display_options['arguments']['field_project_type_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_project_type_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_project_type_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_project_type_value']['limit'] = '0';
$handler->display->display_options['path'] = 'faculty-and-staff/its/projects/feature';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Featured';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
