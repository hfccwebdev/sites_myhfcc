<?php

/**
 * @file
 * Defines the Student Course Information view.
 */

$view = new view();
$view->name = 'student_course_information';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Student Course Information';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Student Program and Course Information';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_subject_area',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Subject Area */
$handler->display->display_options['fields']['field_subject_area']['id'] = 'field_subject_area';
$handler->display->display_options['fields']['field_subject_area']['table'] = 'field_data_field_subject_area';
$handler->display->display_options['fields']['field_subject_area']['field'] = 'field_subject_area';
$handler->display->display_options['fields']['field_subject_area']['label'] = '';
$handler->display->display_options['fields']['field_subject_area']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_subject_area']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_subject_area']['type'] = 'taxonomy_term_reference_plain';
/* Sort criterion: Content: Subject Area (field_subject_area) */
$handler->display->display_options['sorts']['field_subject_area_tid']['id'] = 'field_subject_area_tid';
$handler->display->display_options['sorts']['field_subject_area_tid']['table'] = 'field_data_field_subject_area';
$handler->display->display_options['sorts']['field_subject_area_tid']['field'] = 'field_subject_area_tid';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'class_landing_page' => 'class_landing_page',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'students/classes';
