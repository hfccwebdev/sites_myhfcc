<?php

$view = new view();
$view->name = 'it_projects_report';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'IT Projects Report';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'IT Projects Report';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  4 => '4',
  31 => '31',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'Return to Summary Link';
$handler->display->display_options['header']['area']['content'] = 'Return to [summary report](/faculty-and-staff/its/projects/!1)';
$handler->display->display_options['header']['area']['format'] = 'markdown';
$handler->display->display_options['header']['area']['tokenize'] = TRUE;
/* Field: Content: Link to content */
$handler->display->display_options['fields']['view_node']['id'] = 'view_node';
$handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['view_node']['field'] = 'view_node';
$handler->display->display_options['fields']['view_node']['label'] = '';
$handler->display->display_options['fields']['view_node']['exclude'] = TRUE;
$handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['exclude'] = TRUE;
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['edit_node']['element_wrapper_class'] = 'edit-link';
$handler->display->display_options['fields']['edit_node']['element_default_classes'] = FALSE;
/* Field: Global: Contextual Links */
$handler->display->display_options['fields']['contextual_links']['id'] = 'contextual_links';
$handler->display->display_options['fields']['contextual_links']['table'] = 'views';
$handler->display->display_options['fields']['contextual_links']['field'] = 'contextual_links';
$handler->display->display_options['fields']['contextual_links']['label'] = '';
$handler->display->display_options['fields']['contextual_links']['element_type'] = 'div';
$handler->display->display_options['fields']['contextual_links']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['contextual_links']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['contextual_links']['fields'] = array(
  'view_node' => 'view_node',
  'edit_node' => 'edit_node',
);
$handler->display->display_options['fields']['contextual_links']['check_access'] = 0;
/* Field: Content: Project Health */
$handler->display->display_options['fields']['field_project_health']['id'] = 'field_project_health';
$handler->display->display_options['fields']['field_project_health']['table'] = 'field_data_field_project_health';
$handler->display->display_options['fields']['field_project_health']['field'] = 'field_project_health';
$handler->display->display_options['fields']['field_project_health']['label'] = '';
$handler->display->display_options['fields']['field_project_health']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_project_health']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_project_health']['alter']['text'] = '<img src="/sites/hfcportal/themes/portal/images/project-status-[field_project_health-value].png" alt="[field_project_health]" title="[field_project_health]">';
$handler->display->display_options['fields']['field_project_health']['element_label_colon'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['text'] = '[field_project_health] [title]';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_type'] = 'h3';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: ITS Contact */
$handler->display->display_options['fields']['field_project_its_contact']['id'] = 'field_project_its_contact';
$handler->display->display_options['fields']['field_project_its_contact']['table'] = 'field_data_field_project_its_contact';
$handler->display->display_options['fields']['field_project_its_contact']['field'] = 'field_project_its_contact';
$handler->display->display_options['fields']['field_project_its_contact']['label'] = '';
$handler->display->display_options['fields']['field_project_its_contact']['element_type'] = 'strong';
$handler->display->display_options['fields']['field_project_its_contact']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_project_its_contact']['element_wrapper_type'] = 'div';
/* Field: Content: Progress Indicator */
$handler->display->display_options['fields']['field_project_progress']['id'] = 'field_project_progress';
$handler->display->display_options['fields']['field_project_progress']['table'] = 'field_data_field_project_progress';
$handler->display->display_options['fields']['field_project_progress']['field'] = 'field_project_progress';
$handler->display->display_options['fields']['field_project_progress']['label'] = 'Status';
$handler->display->display_options['fields']['field_project_progress']['element_wrapper_type'] = 'div';
/* Field: Content: Deadline */
$handler->display->display_options['fields']['field_expected_end_date']['id'] = 'field_expected_end_date';
$handler->display->display_options['fields']['field_expected_end_date']['table'] = 'field_data_field_expected_end_date';
$handler->display->display_options['fields']['field_expected_end_date']['field'] = 'field_expected_end_date';
$handler->display->display_options['fields']['field_expected_end_date']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['field_expected_end_date']['empty'] = 'TBD';
$handler->display->display_options['fields']['field_expected_end_date']['settings'] = array(
  'format_type' => 'calendar_short',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Completion Date */
$handler->display->display_options['fields']['field_end_date']['id'] = 'field_end_date';
$handler->display->display_options['fields']['field_end_date']['table'] = 'field_data_field_end_date';
$handler->display->display_options['fields']['field_end_date']['field'] = 'field_end_date';
$handler->display->display_options['fields']['field_end_date']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['field_end_date']['settings'] = array(
  'format_type' => 'calendar_short',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = 'Summary';
$handler->display->display_options['fields']['body']['element_type'] = 'div';
$handler->display->display_options['fields']['body']['element_class'] = 'field-item';
$handler->display->display_options['fields']['body']['element_label_type'] = 'div';
$handler->display->display_options['fields']['body']['element_label_class'] = 'field-label';
$handler->display->display_options['fields']['body']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['body']['element_wrapper_class'] = 'field field-type-text-with-summary field-label-above';
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['label'] = 'Last update';
$handler->display->display_options['fields']['changed']['date_format'] = 'calendar_short';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Sort criterion: Content: Order (field_weight) */
$handler->display->display_options['sorts']['field_weight_value']['id'] = 'field_weight_value';
$handler->display->display_options['sorts']['field_weight_value']['table'] = 'field_data_field_weight';
$handler->display->display_options['sorts']['field_weight_value']['field'] = 'field_weight_value';
/* Sort criterion: Content: Updated date */
$handler->display->display_options['sorts']['changed']['id'] = 'changed';
$handler->display->display_options['sorts']['changed']['table'] = 'node';
$handler->display->display_options['sorts']['changed']['field'] = 'changed';
$handler->display->display_options['sorts']['changed']['order'] = 'DESC';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'it_projects_summary:page_sort';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
/* Contextual filter: Content: Project Type (field_project_type) */
$handler->display->display_options['arguments']['field_project_type_value']['id'] = 'field_project_type_value';
$handler->display->display_options['arguments']['field_project_type_value']['table'] = 'field_data_field_project_type';
$handler->display->display_options['arguments']['field_project_type_value']['field'] = 'field_project_type_value';
$handler->display->display_options['arguments']['field_project_type_value']['default_action'] = 'not found';
$handler->display->display_options['arguments']['field_project_type_value']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['field_project_type_value']['title'] = 'IT Projects Report (%1)';
$handler->display->display_options['arguments']['field_project_type_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_project_type_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_project_type_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_project_type_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_project_type_value']['limit'] = '0';
$handler->display->display_options['filter_groups']['operator'] = 'OR';
$handler->display->display_options['filter_groups']['groups'] = array(
  1 => 'AND',
  2 => 'AND',
);
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'it_project' => 'it_project',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Progress Indicator (field_project_progress) */
$handler->display->display_options['filters']['field_project_progress_value']['id'] = 'field_project_progress_value';
$handler->display->display_options['filters']['field_project_progress_value']['table'] = 'field_data_field_project_progress';
$handler->display->display_options['filters']['field_project_progress_value']['field'] = 'field_project_progress_value';
$handler->display->display_options['filters']['field_project_progress_value']['operator'] = 'not';
$handler->display->display_options['filters']['field_project_progress_value']['value'] = array(
  'G' => 'G',
  'H' => 'H',
);
$handler->display->display_options['filters']['field_project_progress_value']['group'] = 1;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status_1']['id'] = 'status_1';
$handler->display->display_options['filters']['status_1']['table'] = 'node';
$handler->display->display_options['filters']['status_1']['field'] = 'status';
$handler->display->display_options['filters']['status_1']['value'] = '1';
$handler->display->display_options['filters']['status_1']['group'] = 2;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type_1']['id'] = 'type_1';
$handler->display->display_options['filters']['type_1']['table'] = 'node';
$handler->display->display_options['filters']['type_1']['field'] = 'type';
$handler->display->display_options['filters']['type_1']['value'] = array(
  'it_project' => 'it_project',
);
$handler->display->display_options['filters']['type_1']['group'] = 2;
/* Filter criterion: Content: Progress Indicator (field_project_progress) */
$handler->display->display_options['filters']['field_project_progress_value_1']['id'] = 'field_project_progress_value_1';
$handler->display->display_options['filters']['field_project_progress_value_1']['table'] = 'field_data_field_project_progress';
$handler->display->display_options['filters']['field_project_progress_value_1']['field'] = 'field_project_progress_value';
$handler->display->display_options['filters']['field_project_progress_value_1']['value'] = array(
  'G' => 'G',
  'H' => 'H',
);
$handler->display->display_options['filters']['field_project_progress_value_1']['group'] = 2;
/* Filter criterion: Content: Completion Date (field_end_date) */
$handler->display->display_options['filters']['field_end_date_value']['id'] = 'field_end_date_value';
$handler->display->display_options['filters']['field_end_date_value']['table'] = 'field_data_field_end_date';
$handler->display->display_options['filters']['field_end_date_value']['field'] = 'field_end_date_value';
$handler->display->display_options['filters']['field_end_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_end_date_value']['group'] = 2;
$handler->display->display_options['filters']['field_end_date_value']['default_date'] = '-4 weeks';

/* Display: Report Page */
$handler = $view->new_display('page', 'Report Page', 'page');
$handler->display->display_options['path'] = 'faculty-and-staff/its/projects/%/report';
