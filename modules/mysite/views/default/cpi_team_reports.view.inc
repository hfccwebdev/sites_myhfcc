<?php

/**
 * @file
 * Defines the CPI Team Reports view.
 */

$view = new view();
$view->name = 'cpi_team_reports';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'CPI Team Reports';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Team Members and Reports';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['row_class'] = 'clearfix';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
/* Header: Global: View area */
$handler->display->display_options['header']['view']['id'] = 'view';
$handler->display->display_options['header']['view']['table'] = 'views';
$handler->display->display_options['header']['view']['field'] = 'view';
$handler->display->display_options['header']['view']['empty'] = TRUE;
$handler->display->display_options['header']['view']['view_to_insert'] = 'cpi_reporting_term:default';
$handler->display->display_options['header']['view']['inherit_arguments'] = TRUE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'View title';
$handler->display->display_options['header']['area']['content'] = '### Team Members and Reports';
$handler->display->display_options['header']['area']['format'] = 'markdown';
/* Relationship: Content: Taxonomy terms on node */
$handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
$handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
$handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
$handler->display->display_options['relationships']['term_node_tid']['label'] = 'Reporting Term';
$handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
  'cpi_reporting_terms' => 'cpi_reporting_terms',
  'catalog_master_content_categories' => 0,
  'file_categories' => 0,
  'new_catalog_categories' => 0,
  'userpoints' => 0,
  'wc_kb_tags' => 0,
);
/* Field: Content: Team Photo */
$handler->display->display_options['fields']['field_cpi_team_photo']['id'] = 'field_cpi_team_photo';
$handler->display->display_options['fields']['field_cpi_team_photo']['table'] = 'field_data_field_cpi_team_photo';
$handler->display->display_options['fields']['field_cpi_team_photo']['field'] = 'field_cpi_team_photo';
$handler->display->display_options['fields']['field_cpi_team_photo']['label'] = '';
$handler->display->display_options['fields']['field_cpi_team_photo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_cpi_team_photo']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['field_cpi_team_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_cpi_team_photo']['settings'] = array(
  'image_style' => 'teaser',
  'image_link' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_wrapper_type'] = 'h4';
$handler->display->display_options['fields']['title']['element_wrapper_class'] = 'title';
$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
/* Field: Content: Team Report */
$handler->display->display_options['fields']['field_cpi_team_report']['id'] = 'field_cpi_team_report';
$handler->display->display_options['fields']['field_cpi_team_report']['table'] = 'field_data_field_cpi_team_report';
$handler->display->display_options['fields']['field_cpi_team_report']['field'] = 'field_cpi_team_report';
$handler->display->display_options['fields']['field_cpi_team_report']['label'] = '';
$handler->display->display_options['fields']['field_cpi_team_report']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_cpi_team_report']['alter']['text'] = 'Download Report';
$handler->display->display_options['fields']['field_cpi_team_report']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_cpi_team_report']['alter']['path'] = '[field_cpi_team_report]';
$handler->display->display_options['fields']['field_cpi_team_report']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_cpi_team_report']['element_wrapper_type'] = 'p';
$handler->display->display_options['fields']['field_cpi_team_report']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_cpi_team_report']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_cpi_team_report']['type'] = 'file_url_plain';
$handler->display->display_options['fields']['field_cpi_team_report']['settings'] = array(
  'text' => 'Download Report',
);
/* Field: Content: Team Members */
$handler->display->display_options['fields']['field_cpi_team_members']['id'] = 'field_cpi_team_members';
$handler->display->display_options['fields']['field_cpi_team_members']['table'] = 'field_data_field_cpi_team_members';
$handler->display->display_options['fields']['field_cpi_team_members']['field'] = 'field_cpi_team_members';
$handler->display->display_options['fields']['field_cpi_team_members']['label'] = '';
$handler->display->display_options['fields']['field_cpi_team_members']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_cpi_team_members']['element_wrapper_type'] = 'p';
$handler->display->display_options['fields']['field_cpi_team_members']['element_default_classes'] = FALSE;
/* Field: Content: Additional Files */
$handler->display->display_options['fields']['field_cpi_additional_files']['id'] = 'field_cpi_additional_files';
$handler->display->display_options['fields']['field_cpi_additional_files']['table'] = 'field_data_field_cpi_additional_files';
$handler->display->display_options['fields']['field_cpi_additional_files']['field'] = 'field_cpi_additional_files';
$handler->display->display_options['fields']['field_cpi_additional_files']['element_type'] = 'div';
$handler->display->display_options['fields']['field_cpi_additional_files']['element_label_type'] = 'div';
$handler->display->display_options['fields']['field_cpi_additional_files']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['field_cpi_additional_files']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_cpi_additional_files']['delta_offset'] = '0';
/* Field: Content: Edit link */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['edit_node']['element_wrapper_class'] = 'edit-link';
/* Sort criterion: Taxonomy term: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['relationship'] = 'term_node_tid';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Contextual filter: Content: Has taxonomy term ID */
$handler->display->display_options['arguments']['tid']['id'] = 'tid';
$handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
$handler->display->display_options['arguments']['tid']['field'] = 'tid';
$handler->display->display_options['arguments']['tid']['default_action'] = 'default';
$handler->display->display_options['arguments']['tid']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['tid']['title'] = 'Continuous Process Improvement Team Members and Reports: %1';
$handler->display->display_options['arguments']['tid']['default_argument_type'] = 'php';
$handler->display->display_options['arguments']['tid']['default_argument_options']['code'] = 'if (function_exists(\'mysite_get_cpi_default_term\')) {
  return mysite_get_cpi_default_term();
}
';
$handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
$handler->display->display_options['arguments']['tid']['validate_options']['vocabularies'] = array(
  'cpi_reporting_terms' => 'cpi_reporting_terms',
);
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'cpi_team_report' => 'cpi_team_report',
);

/* Display: Reports Page */
$handler = $view->new_display('page', 'Reports Page', 'page_archive');
$handler->display->display_options['display_description'] = 'Displays reports for the specified term. Defaults to most recent term.';
$handler->display->display_options['path'] = 'cpi/reports/term';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'Team Reports';
$handler->display->display_options['menu']['weight'] = '-10';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'Team Members and Reports';
$handler->display->display_options['tab_options']['weight'] = '0';
$handler->display->display_options['tab_options']['name'] = 'menu-continuous-process-improvem';

/* Display: Archive Page */
$handler = $view->new_display('page', 'Archive Page', 'page_1');
$handler->display->display_options['display_description'] = 'Displays a list of prior terms with report titles for each term';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'list';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'name',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Taxonomy term: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'term_node_tid';
$handler->display->display_options['fields']['name']['label'] = '';
$handler->display->display_options['fields']['name']['exclude'] = TRUE;
$handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['name']['element_wrapper_type'] = 'h3';
$handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
$handler->display->display_options['path'] = 'cpi/reports/archive';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Previous Terms';
$handler->display->display_options['menu']['weight'] = '10';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
