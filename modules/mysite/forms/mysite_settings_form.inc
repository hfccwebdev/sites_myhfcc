<?php

/**
 * @file
 * Contains the site settings form.
 */

/**
 * Site settings form.
 */
function mysite_settings_form($form, &$form_state) {

  $form['site_options'] = [
    '#type' => 'fieldset',
    '#title' => t('Site options'),
  ];

  $form['site_options']['mysite_experience_redirect'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable Experience Redirect'),
    '#default_value' => variable_get('mysite_experience_redirect', FALSE),
    '#description' => t('Disable this to show local homepage instead.'),
  ];

  $form['reportviewer'] = [
    '#type' => 'fieldset',
    '#title' => t('SQL Server ReportViewer links'),
  ];

  $form['reportviewer']['reportviewer_uri'] = [
    '#title' => t('SQL Server ReportViewer URI'),
    '#type' => 'textfield',
    '#size' => 128,
    '#default_value' => variable_get('reportviewer_uri', NULL),
    '#required' => TRUE,
  ];

  $form['reportviewer']['reportviewer_map'] = [
    '#title' => t('ReportViewer path mapping'),
    '#type' => 'textarea',
    '#default_value' => variable_get('reportviewer_map', NULL),
    '#description' => t('The possible reports that can be redirected. Enter one value per line, in the format path|report.'),
    '#required' => TRUE,
  ];

  if ($known_reports = list_extract_allowed_values(variable_get('reportviewer_map', ''), 'list_text', FALSE)) {
    $test_links = [];
    foreach ($known_reports as $key => $report) {
      $test_links[] = l($report, "faculty-and-staff/report-viewer/$key", ['attributes' => ['target' => '_blank']]);
    }
    $form['reportviewer']['test_links'] = [
      '#title' => t('ReportViewer test links'),
      '#theme' => 'item_list',
      '#items' => $test_links,
    ];
  }

  $form['sarslinks'] = [
    '#type' => 'fieldset',
    '#title' => t('SARS login links'),
    '#description' => t('SARS login links are <strong>defined in code</strong>. Use these links for testing.'),
  ];

  $link_options = ['attributes' => ['target' => '_blank']];

  $form['sarslinks']['advising'] = [
    '#title' => t('Advising links'),
    '#theme' => 'item_list',
    '#items' => [
      l(t('Ask a question'), 'students/advising/question', $link_options),
      l(t('Schedule remote appointment'), 'students/advising/schedule', $link_options),
      l(t('Schedule on-campus appointment'), 'students/advising/schedule-oc', $link_options),
      l(t('Learn4ward'), 'students/advising/learn4ward', $link_options),
    ],
  ];

  $form['sarslinks']['counseling'] = [
    '#title' => t('Counseling links'),
    '#theme' => 'item_list',
    '#items' => [
      l(t('Ask a question'), 'students/counseling/question', $link_options),
      l(t('Schedule remote appointment'), 'students/counseling/schedule', $link_options),
      l(t('Schedule on-campus appointment'), 'students/counseling/schedule-oc', $link_options),
    ],
  ];

  return system_settings_form($form);
}
