<?php

/**
 * @file
 * Token callbacks for the mysite module.
 */

/**
 * Implements hook_token_info().
 */
function mysite_token_info() {
  $info = [];

  // Add any new tokens.
  $info['tokens']['node'] = [
    'og_group_ref_nid' => [
      'name' => t('Groups audience ID'),
      'description' => t('Node ID of the referenced group.'),
    ],
    'og_group_ref_path' => [
      'name' => t('Groups audience path'),
      'description' => t('Path alias of the referenced group.'),
    ],
  ];

  // Return them.
  return $info;
}

/**
 * Implements hook_tokens().
 */
function mysite_tokens($type, $tokens, array $data = [], array $options = []) {
  $replacements = [];
  if ($type == 'node') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'og_group_ref_nid':
          if (!empty($data['node']->og_group_ref)) {
            $target = reset($data['node']->og_group_ref[LANGUAGE_NONE]);
            $replacements[$original] = $target['target_id'];
          }
          break;
        case 'og_group_ref_path':
          if (!empty($data['node']->og_group_ref)) {
            $target = reset($data['node']->og_group_ref[LANGUAGE_NONE]);
            $replacements[$original] = drupal_get_path_alias('node/' . $target['target_id']);
          }
          break;
      }
    }
  }
  return $replacements;
}
