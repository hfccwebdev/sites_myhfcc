<?php

/**
 * Defines the SiteUtils class.
 */
class SiteUtils {

  /**
   * Returns a list of Organic Groups as options.
   */
  public static function groupOpts() {
    $results = db_query("SELECT nid, title FROM {node} WHERE type='landing_page' ORDER BY title")->fetchAll();
    $rows = [];
    foreach($results as $node) {
      $rows[$node->nid] = check_plain($node->title);
    }
    return $rows;
  }

  /**
   * Get default CPI reporting term.
   *
   * This can be used to provide a default views argument.
   */
  public static function getCpiDefaultTerm() {
    $vocabulary = taxonomy_vocabulary_machine_name_load('cpi_reporting_terms');
    $query = new EntityFieldQuery();
    $result = $query
      ->entityCondition('entity_type', 'taxonomy_term')
      ->propertyCondition('vid', (int) $vocabulary->vid, '=')
      ->propertyOrderBy('weight')
      ->range(0,1)
      ->execute();
    if (!empty($result['taxonomy_term'])) {
      $terms = array_keys($result['taxonomy_term']);
      return reset($terms);
    }
  }

  /**
   * Get program options for actual graduation programs only.
   *
   * @see hfccwsclient_get_programs_opts()
   */
  public static function getGraduationProgramOpts() {
    $programs = WebServicesClient::getPrograms();
    $program_types = WebServicesClient::getProgramTypes();
    $options = [];
    foreach ($programs as $id => $program) {
      if (!in_array($program['degree_type_raw'], ['AREA', 'EA', 'NODG'])) {
        $options[$id] = $program['title'];
      }
    }
    asort($options);
    return $options;
  }
}
