<?php

/**
 * Defines the SiteBreadcrumbs class.
 */
class SiteBreadcrumbs {

  /**
   * Implements hook_menu_breadcrumb_alter().
   */
  public static function alter(&$active_trail, $item) {
    if (!drupal_is_front_page()) {
      // quick fix. If breadcrumb will only include "Home" then nuke it.
      // @todo: Make useful breadcrumbs for things not handled by Custom Breadcrumbs module. Maybe path-based?
      switch ($item['page_callback']) {
        case 'node_page_view':
          // If $active_trail has already been emptied by someone else,
          // we're going to have problems here, so let's start over.
          if (empty($active_trail)) {
            $active_trail = menu_get_active_trail();
          }
          $node = $item['page_arguments'][0];
          switch ($node->type) {
            case 'committee':
            case 'landing_page':
              if (!empty($node->field_parent_group)) {
                $end = end($active_trail);
                $active_trail = self::buildGroupTrail($node->field_parent_group[LANGUAGE_NONE][0]['target_id']);
                $active_trail[] = $end;
              }
              break;
            case 'page':
              if (!empty($node->og_group_ref)) {
                $end = end($active_trail);
                $active_trail = self::buildGroupTrail($node->og_group_ref[LANGUAGE_NONE][0]['target_id']);
                $active_trail[] = $end;
              }
              break;
          }
          break;
      }
      $count = count($active_trail);
      if ($count <= 2) {
        $active_trail = [];
      }
    }
  }

  /**
   * Build a new breadcrumb trail from parent groups.
   */
  private static function buildGroupTrail($nid) {
    $trail[] = [
      'title' => t('Home'),
      'href' => '<front>',
      'link_path' => '',
      'localized_options' => [],
      'type' => 0,
    ];

    if ($node = node_load($nid)) {
      if (!empty($node->field_parent_group)) {
        $parent = $node->field_parent_group[LANGUAGE_NONE][0]['target_id'];
        if ($parent == $nid) {
          drupal_set_message(t('Do not set landing page as its own parent.'), 'warning');
        }
        else {
          $trail = self::buildGroupTrail($parent);
        }
      }

      $trail[] = [
        'title' => $node->title,
        'href' => 'node/' . $node->nid,
        'link_path' => '',
        'localized_options' => [],
        'type' => 0,
      ];
    }
    else {
      watchdog('mysite', 'Error loading node %nid building group breadcrumb trail.', ['%nid' => $nid], WATCHDOG_ERROR);
    }
    return $trail;
  }


}
