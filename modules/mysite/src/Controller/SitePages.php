<?php

/**
 * Defines the SitePages class.
 */
class SitePages {

  public const ELLUCIAN_EXPERIENCE = 'https://experience.elluciancloud.com/hfc/';

  /**
   * Callback for Front Page.
   */
  public static function frontPage() {
    if (variable_get('mysite_experience_redirect', FALSE)) {
      drupal_goto(self::ELLUCIAN_EXPERIENCE);
      drupal_exit();
    }
    drupal_set_title('');
    return [];
  }

}
