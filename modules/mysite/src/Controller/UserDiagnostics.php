<?php

/**
 * Defines the UserDiagnostics class.
 */
class UserDiagnostics {

  /**
   * Display user diagnostics.
   *
   * param StdClass $account
   *   The User Account
   *
   * return array
   *   A renderable array.
   */
  public static function view($account) {

    $output = [];

    $output['mysite_user_fields'] = [
      'title' => ['#prefix' => '<h2>', '#markup' => t('Drupal User Entity Fields'), '#suffix' => '</h2>'],
      '#weight' => 6,
    ];

    $rows = [];
    foreach (['field_user_first_name', 'field_user_last_name', 'field_user_hank_id', 'field_user_constituency', 'field_user_network_role'] as $fieldname) {
        if (!empty($account->$fieldname)) {
          $rows[] = t('@name: @value', ['@name' => $fieldname, '@value' => $account->{$fieldname}['und'][0]['safe_value']]);
        }
    }
    $output['mysite_user_fields']['items'] = ['#theme' => 'item_list', '#items' => $rows];

    $output['mysite_user_roles'] = [
      'title' => ['#prefix' => '<h2>', '#markup' => t('Drupal Roles'), '#suffix' => '</h2>'],
      '#weight' => 7,
    ];

    $rows = [];
    foreach ($account->roles as $role) {
      $rows[] = check_plain($role);
    }
    $output['mysite_user_roles']['items'] = [
      '#theme' => 'item_list',
      '#items' => $rows,
    ];

    $output['mysite_user_groups'] = [
      'title' => ['#prefix' => '<h2>', '#markup' => t('Organic Groups membership'), '#suffix' => '</h2>'],
      '#weight' => 8,
    ];

    if (!empty($account->og_user_node)) {
      $rows = [];
      foreach ($account->og_user_node['und'] as $item) {
        $title = db_query("SELECT title FROM {node} WHERE nid = :nid", [":nid" => $item['target_id']])->fetchField();
        $rows[] = l($title, 'node/' . $item['target_id']);
      }
      $output['mysite_user_groups']['items'] = [
        '#theme' => 'item_list',
        '#items' => $rows,
      ];
    }

    $output['mysite_user_authmap'] = [
      'title' => ['#prefix' => '<h2>', '#markup' => t('Drupal Authmap'), '#suffix' => '</h2>'],
      '#weight' => 9,
    ];

    $query = "SELECT * FROM {authmap} WHERE uid=:uid OR authname=:name";
    $params = [':uid' => $account->uid, ':name' => $account->name];
    $results = db_query($query, $params)->fetchAll();
    // if (function_exists('dpm')) {
    //   dpm($results, 'results');
    // }
    $found = !empty($results) ? 'found' : 'not found';
    $output['mysite_user_authmap']['info'] = [
      '#theme' => 'item_list',
      '#items' => [t('authmap info @found', ['@found' => $found])],
    ];

    $output['mysite_user_attributes'] = [
      'title' => ['#prefix' => '<h2>', '#markup' => t('LDAP Attributes'), '#suffix' => '</h2>'],
      '#weight' => 10,
    ];

    if (!empty($account->data['ldap_user']['init']['sid'])) {
      $sid = $account->data['ldap_user']['init']['sid'];
      $ldap_server = ldap_servers_get_servers($sid, 'all', TRUE);
      // if (function_exists('dpm')) {
      //   dpm($ldap_server, 'ldap_server');
      // }

      $user_ldap_entry = FALSE;
      $ldap_user_conf = ldap_user_conf();
      if ($ldap_user_conf->drupalAcctProvisionServer) {
        $user_ldap_entry = ldap_servers_get_user_ldap_data($account->name, $ldap_user_conf->drupalAcctProvisionServer);
      }
      if ($ldap_user_conf->ldapEntryProvisionServer) {
        if (!$user_ldap_entry) {
          $user_ldap_entry = ldap_servers_get_user_ldap_data($account->name, $ldap_user_conf->ldapEntryProvisionServer);
        }
      }
      if ($user_ldap_entry) {
        // if (function_exists('dpm')) {
        //   dpm($user_ldap_entry, 'user_ldap_entry');
        // }
        $rows = [];
        foreach (explode(',', $ldap_server->groupUserMembershipsAttr) as $attribute) {
          $item = ['data' => $attribute];
          if (!empty($user_ldap_entry['attr'][$attribute])) {
            for ($c = 0; $c < $user_ldap_entry['attr'][$attribute]['count']; $c++) {
              $item['children'][$c] = $user_ldap_entry['attr'][$attribute][$c];
            }
          }
          $rows[] = $item;
        }
        $output['mysite_user_attributes']['items'] = [
          '#theme' => 'item_list',
          '#items' => $rows,
        ];
      }
    }

    drupal_alter('mysite_user_diagnostics', $output, $account);

    return $output;
  }
}
