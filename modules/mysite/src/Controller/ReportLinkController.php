<?php

/**
 * Defines the ReportLinkController.
 *
 * This controller uses info from the HankTools service to inject an encrypted
 * HANK ID into the redirection target.
 */
class ReportLinkController {

  /**
   * Generates the link arguments.
   */
  public static function generate($report_name, $hank_id =  NULL) {

    if (!module_exists('myhank')) {
      drupal_set_message(t('HankTools not currently available. Please try again later.'), 'warning');
      return;
    }

    $hank_id = !empty($hank_id) ? $hank_id : HankTools::myHankId();

    $known_reports = list_extract_allowed_values(variable_get('reportviewer_map', ''), 'list_text', FALSE);

    if (!empty($known_reports[$report_name]) && $eid = HankTools::create($hank_id)->eid()) {
      return urlencode($known_reports[$report_name]) . "&FACULTYID={$eid}";
    }
  }

  /**
   * Page callback.
   *
   * On success, this page will redirect to the College Store website.
   */
  public static function view($report_name, $hank_id = NULL) {

    if ($args = self::generate($report_name, $hank_id)) {
      $target = variable_get('reportviewer_uri', NULL) . "?" . $args;
      drupal_goto($target);
    }
    else {
      drupal_set_title(t('ReportViewer'));
      return ['#markup' => t("Sorry, could not generate redirect info for the requested report.")];
    }
  }
}
