<?php

/**
 * Defines the SarsLinkController.
 *
 * This controller uses info from the HankTools service to inject an encrypted
 * HANK ID into the redirection target.
 */
class SarsLinkController {

  /**
   * Defines the eAdvising login URI.
   */
  const ADVG_QUESTION = 'https://sars.hfcc.edu/eAdvising/hfc-login.html';

  /**
   * Defines the Appointment Scheduling login URI.
   */
  const ADVG_SCHED_REMOTE = 'https://sars.hfcc.edu/eSARSRedirect/hfc-login.html';

  /**
   * Defines the On-Campus Appointment Scheduling login URI.
   */
  const ADVG_SCHED_OC = 'https://sars.hfcc.edu/eSARSRedirect_OC/hfc-login.html';

  /**
   * Defines the Counseling "Ask a Question" link.
   */
  const COUN_QUESTION = 'https://sars.hfcc.edu/eCounseling/hfc-login.html';

  /**
   * Defines the Counseling remote schedule link.
   */
  const COUN_SCHED_REMOTE = 'https://sars.hfcc.edu/eSARSRedirect_Counseling/hfc-login.html';

  /**
   * Defines the Counseling on-campus schedule link.
   */
  const COUN_SCHED_OC = 'https://sars.hfcc.edu/eSARSRedirect_Counseling_OC/hfc-login.html';

  /**
   * Defines the Learn4ward login link.
   */
  const LEARN4WARD = 'https://sars.hfcc.edu/eSARSRedirect_StudentSuccess/hfc-login.html';

  /**
   * Generates the link arguments.
   */
  public static function generate($department, $report_name, $hank_id = NULL) {

    if (!module_exists('myhank')) {
      drupal_set_message(t('HankTools not currently available. Please try again later.'), 'warning');
      return;
    }

    $hank_id = !empty($hank_id) ? $hank_id : HankTools::myHankId();

    if ($eid = HankTools::create($hank_id)->eid()) {
      switch ("$department/$report_name") {
        case 'advising/question':
          return self::ADVG_QUESTION . "?ID={$eid}";

        case 'advising/schedule':
          return self::ADVG_SCHED_REMOTE . "?ID={$eid}";

        case 'advising/schedule-oc':
          return self::ADVG_SCHED_OC . "?ID={$eid}";

        case 'advising/learn4ward':
          return self::LEARN4WARD . "?ID={$eid}";

        case 'counseling/question':
          return self::COUN_QUESTION . "?ID={$eid}";

        case 'counseling/schedule':
          return self::COUN_SCHED_REMOTE . "?ID={$eid}";

        case 'counseling/schedule-oc':
          return self::COUN_SCHED_OC . "?ID={$eid}";

      }
    }
  }

  /**
   * Page callback.
   *
   * On success, this page will redirect to the College Store website.
   */
  public static function view($report_name, $hank_id = NULL) {
    if ($target = self::generate($report_name, $hank_id)) {
      drupal_goto($target);
    }
    else {
      drupal_set_title(t('SARS Anywhere'));
      return ['#markup' => t("Sorry, could not generate redirect info for the requested resource.")];
    }
  }

}
