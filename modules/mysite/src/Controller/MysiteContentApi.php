<?php

/**
 * Provides the HFC News 1.1 API.
 *
 * All api/1.0 endpoints are provided by the
 * Services and Services Views contrib modules.
 */
class MysiteContentApi {

  /**
   * Returns the full news index.
   */
  public static function index($type): void {

    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;

    self::checkApiKey();

    $result = db_query("
      SELECT nid, uuid, changed FROM {node}
      WHERE status = 1 AND type = :type
    ", [':type' => $type])
      ->fetchAllAssoc('nid');
    drupal_json_output($result);
    drupal_exit();
  }

  /**
   * Returns news content.
   */
  public static function content(object $node): void {

    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;

    self::checkApiKey();

    unset($node->data);

    // Convert all file URIs.
    if (!empty($node->field_news_photo)) {
      self::convertFileUri($node->field_news_photo);
    }

    if (!empty($node->field_gallery_photos)) {
      self::convertFileUri($node->field_gallery_photos);
    }

    if (!empty($node->field_attachments)) {
      self::convertFileUri($node->field_attachments);
    }

    if (!empty($node->field_news_attachments)) {
      self::convertFileUri($node->field_news_attachments);
    }

    // File attachments for TIF proposals.

    foreach (['field_proposal', 'field_board_report', 'field_final_report'] as $fieldname)  {
      if (!empty($node->{$fieldname})) {
        self::convertFileUri($node->{$fieldname});
      }
  }

    // Load child paragraph entities.
    if (!empty($node->field_paragraphs)) {
      self::loadParagraphs($node->field_paragraphs);
    }

    drupal_json_output($node);
    drupal_exit();
  }

  /**
   * Returns the user index.
   */
  public static function users(): void {

    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;

    self::checkApiKey();

    $result = db_query("
      SELECT u.uid, u.name, u.mail, u.status FROM {users} u
      JOIN {node} n ON n.uid = u.uid
      WHERE u.uid > 0 AND u.status = 1
      GROUP BY u.uid ORDER BY u.uid
    ")->fetchAllAssoc('uid');
    drupal_json_output($result);
    drupal_exit();
  }

  /**
   * File index.
   */
  public static function fileIndex(): void {

    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;

    self::checkApiKey();

    $result = db_query("
      SELECT fid, uri, uuid, `timestamp` FROM {file_managed}
      ORDER BY uri
    ")->fetchAllAssoc('fid');
    drupal_json_output($result);
    drupal_exit();
  }

  /**
   * Returns a file object.
   */
  public static function file(object $file): void {

    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;

    self::checkApiKey();

    if (file_exists($file->uri)) {
      $file->url = file_create_url($file->uri, ['absolute' => TRUE]);
      $file->sha1 = sha1_file($file->uri);
      drupal_json_output($file);
      drupal_exit();
    }
    drupal_not_found();
    drupal_exit();
  }

  /**
   * Convert file URIs.
   */
  private static function convertFileUri(&$field): void {
    foreach ($field[LANGUAGE_NONE] as $key => $file) {
      $url = file_create_url($file['uri'], ['absolute' => TRUE]);
      $field[LANGUAGE_NONE][$key]['uri'] = $url;
    }
  }

  /**
   * Load child paragraph entities and convert file URIs.
   */
  private static function loadParagraphs(&$paragraphs): void {
    foreach ($paragraphs[LANGUAGE_NONE] as $key => $metadata) {

      $paragraph = paragraphs_item_revision_load($metadata['revision_id']);

      if (!empty($paragraph->field_para_background_photo)) {
        self::convertFileUri($paragraph->field_para_background_photo);
      }

      if (!empty($paragraph->field_para_caption_photos)) {
        self::convertFileUri($paragraph->field_para_caption_photos);
      }

      if (!empty($paragraph->field_para_photos)) {
        self::convertFileUri($paragraph->field_para_photos);
      }

      if (!empty($paragraph->field_person_photo)) {
        self::convertFileUri($paragraph->field_person_photo);
      }

      if (!empty($paragraph->field_paragraphs)) {
        self::loadParagraphs($paragraph->field_paragraphs);
      }

      if (!empty($paragraph->field_para_nested)) {
        self::loadParagraphs($paragraph->field_para_nested);
      }

      $paragraphs[LANGUAGE_NONE][$key] = $paragraph;
    }
  }

  /**
   * Check the api key.
   */
  private static function checkApiKey(): void {

    $apikey = getallheaders()['Apikey'] ?? NULL;
    if (is_null($apikey)) {
      drupal_add_http_header('Status', '403 Access Denied');
      drupal_json_output(['error' => 'could not read api key.']);
      drupal_exit();
    }
    elseif (trim($apikey) !== variable_get('mysite-api-secret')) {
      drupal_add_http_header('Status', '403 Access Denied');
      drupal_json_output(['error' => 'invalid api key']);
      drupal_exit();
    }
  }

}
