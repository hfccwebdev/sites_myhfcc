<?php

/**
 * Defines the SiteTitleBlock.
 */
class SiteTitleBlock {

  /**
   * Instantiates a new object of this class.
   */
  public static function create() {
    $class = get_called_class();
    return new $class();
  }

  /**
   * Returns the block label.
   */
  public function label() {
    return NULL;
  }

  /**
   * Returns values for hook_block_info().
   */
  public function info() {
    return ['info' => t('Site Title Block'), 'cache' => DRUPAL_CACHE_PER_PAGE];
  }

  /**
   * Returns form for hook_block_configure().
   */
  public function configure() {
    $form = [];
    return $form;
  }

  /**
   * Saves configuration for hook_block_save().
   */
  public function save($edit) {
  }

  /**
   * Returns value for hook_block_view().
   */
  public function view() {
    $output[] = ['#markup' => l(t('HFC Portal'), '<front>')];
    return ['subject' => $this->label(), 'content' => $output];
  }
}
