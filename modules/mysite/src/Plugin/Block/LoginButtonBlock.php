<?php

/**
 * Defines the LoginButtonBlock class.
 */
class LoginButtonBlock {

  /**
   * Instantiates a new object of this class.
   */
  public static function create() {
    $class = get_called_class();
    return new $class();
  }

  /**
   * Returns the block label.
   */
  public function label() {
    return t('Log in to HFC Portal');
  }

  /**
   * Returns values for hook_block_info().
   */
  public function info() {
    return ['info' => t('Login Block'), 'cache' => DRUPAL_NO_CACHE];
  }

  /**
   * Returns form for hook_block_configure().
   */
  public function configure() {
    $form = [];
    return $form;
  }

  /**
   * Saves configuration for hook_block_save().
   */
  public function save($edit) {
  }

  /**
   * Returns value for hook_block_view().
   */
  public function view() {
    $options = ['attributes' => ['class' => ['hfc-button', 'hfc-button-primary']]];

    $output = [];
    $output[] = [
      '#prefix' => '<p>',
      '#markup' => l('Password Help', 'https://my.hfcc.edu/password', $options),
      '#suffix' => '</p>',
      '#weight' => 10,
    ];

    if ($destination = drupal_get_destination()) {
      $options['query'] = $destination;
    }

    $output[] = [
      '#prefix' => '<p>',
      '#markup' => l('Log In', 'user/login', $options),
      '#suffix' => '</p>',
      '#weight' => -10,
    ];

    return ['subject' => $this->label(), 'content' => $output];
  }
}
