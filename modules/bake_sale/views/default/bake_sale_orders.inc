<?php

$view = new view();
$view->name = 'bake_sale_orders';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'entityform';
$view->human_name = 'Fifty-One O One Bake Sale Orders';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Bake Sale Orders';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'view bake sale reports';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_pick_up_date',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Entityform Submission: Pick-up Date */
$handler->display->display_options['fields']['field_pick_up_date']['id'] = 'field_pick_up_date';
$handler->display->display_options['fields']['field_pick_up_date']['table'] = 'field_data_field_pick_up_date';
$handler->display->display_options['fields']['field_pick_up_date']['field'] = 'field_pick_up_date';
$handler->display->display_options['fields']['field_pick_up_date']['label'] = 'Pick-up on';
$handler->display->display_options['fields']['field_pick_up_date']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_pick_up_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_pick_up_date']['element_wrapper_type'] = 'h2';
/* Field: Entityform Submission: Rendered Entityform Submission */
$handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
$handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_entityform';
$handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
$handler->display->display_options['fields']['rendered_entity']['label'] = '';
$handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
$handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
$handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'full';
$handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
/* Sort criterion: Entityform Submission: Pick-up Date (field_pick_up_date) */
$handler->display->display_options['sorts']['field_pick_up_date_value']['id'] = 'field_pick_up_date_value';
$handler->display->display_options['sorts']['field_pick_up_date_value']['table'] = 'field_data_field_pick_up_date';
$handler->display->display_options['sorts']['field_pick_up_date_value']['field'] = 'field_pick_up_date_value';
/* Sort criterion: Entityform Submission: Full Name (field_full_name) */
$handler->display->display_options['sorts']['field_full_name_value']['id'] = 'field_full_name_value';
$handler->display->display_options['sorts']['field_full_name_value']['table'] = 'field_data_field_full_name';
$handler->display->display_options['sorts']['field_full_name_value']['field'] = 'field_full_name_value';
/* Filter criterion: Entityform Submission: Entityform Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'entityform';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'hfc_bake_sale_pre_order_form' => 'hfc_bake_sale_pre_order_form',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = '5101/bake-sale/orders';
